package ua.kharkiv.khpi.kalin.task03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
import ua.kharkiv.khpi.kalin.task02.TrajectoryCalc;

/**
 * The Class ViewResult.
 */
public class ViewResult implements View {

	/** The Constant FNAME. */
	private static final String FNAME = "items.data";
	
	/** The Constant DEFAULT_NUM. */
	private static final int DEFAULT_NUM = 7;
	
	/** The items. */
	private ArrayList<TrajectoryParams> items = 
			new ArrayList<TrajectoryParams>();
	
	/**
	 * Instantiates a new view result.
	 */
	public ViewResult() {
		this(DEFAULT_NUM);
	}
		
	/**
	 * Instantiates a new view result.
	 *
	 * @param n the n
	 */
	public ViewResult(int n) {
		for(int ctr = 0; ctr < n; ctr++) {
			items.add(new TrajectoryParams());
		}
	}
		
	/**
	 * Gets the items.
	 *
	 * @return the items
	 */
	public ArrayList<TrajectoryParams> getItems() {
		return items;
	}
		
	/**
	 * Inits the.
	 *
	 * @param step_angle the step angle
	 * @param step_velocity the step velocity
	 * @param step_name the step name
	 */
	public void init(double step_angle, 
			double step_velocity, int step_name) {
		
		double angle    = 0.0;
		double velocity = 0.0;
		
		for(TrajectoryParams item : items) {
			
			item.setAngle(angle);
			item.setVelocity(velocity);
			item.setName("Path calc #" + step_name);
			
			TrajectoryCalc calc_item = new TrajectoryCalc(item);
			calc_item.calcAndSavePath();
			
			angle    += step_angle;
			velocity += step_velocity;
			step_name++;
		}
	}
		
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.View#viewInit()
	 */
	@Override
	public void viewInit() {
		init(Math.random() * 180.0, 
				Math.random() * 90.0, 
				(int)Math.random()+1);
	}
		
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.View#viewSave()
	 */
	@Override
	public void viewSave() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(
				new FileOutputStream(FNAME));
		os.writeObject(items);
		os.flush();
		os.close();
	}
		
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.View#viewRestore()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void viewRestore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(
				new FileInputStream(FNAME));
		items = (ArrayList<TrajectoryParams>) is.readObject();
		is.close();
	}
		
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.View#viewHeader()
	 */
	@Override
	public void viewHeader() {
		System.out.println("Results:");
	}
		
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.View#viewBody()
	 */
	@Override
	public void viewBody() {
		for(TrajectoryParams item : items) {
			System.out.printf("\n %s --> "
					+ "(Angle: %.3f; Velocity: %.3f; Path: %.3f) ",
			item.getName(), item.getAngle(), 
			item.getVelocity(), item.getPath());
		}
		System.out.println();
	}
		
		
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.View#viewFooter()
	 */
	@Override
	public void viewFooter() {
		System.out.println("End.");
	}
		
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.View#viewShow()
	 */
	@Override
	public void viewShow() {
		viewHeader();
		viewBody();
		viewFooter();
	}
}
