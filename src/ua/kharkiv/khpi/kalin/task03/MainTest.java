package ua.kharkiv.khpi.kalin.task03;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import ua.kharkiv.khpi.kalin.*;
import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
// TODO: Auto-generated Javadoc
/**
 * The Class TrajectoryTests.
 */
public class MainTest {
	
	/**
	 * Test calc.
	 */
	@Test
	public void testCalc() {
		ViewResult view = new ViewResult(2);
		
		view.init(200.0, 120.0, 15);
		
		TrajectoryParams item = new TrajectoryParams();
		
		int ctr = 1;
		
		item.setAngle(200.0);
		item.setVelocity(120.0);
		item.setName("Path calc #15");
		
		assertEquals(item.getAngle(), view.getItems().get(ctr).getAngle(), 
				item.getAngle() + " should be equal to " + 
				view.getItems().get(ctr).getAngle());
		
		assertEquals(item.getVelocity(), 
				view.getItems().get(ctr).getVelocity(), 
				item.getAngle() + " should be equal to " + 
				view.getItems().get(ctr).getVelocity());
		
		assertEquals(item.getName(), view.getItems().get(ctr-1).getName(), 
				item.getName() + " should be equal to " + view.getItems().get(ctr-1).getName());
	}
	
	/**
	 * Test restore.
	 */
	@Test
	public void testRestore() {
		ViewResult view1 = new ViewResult(1000);
		ViewResult view2 = new ViewResult();
		
		view1.init(Math.random() * 180.0, 
				Math.random() * 90.0, 
				(int)Math.random()+1);
		
		try {
			view1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		
		try {
			view2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
