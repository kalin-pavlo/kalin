package ua.kharkiv.khpi.kalin.task03;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewableResult.
 */
public class ViewableResult implements Viewable {

	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.Viewable#getView()
	 */
	@Override
	public View getView() {
		return new ViewResult();
	}
}
