package ua.kharkiv.khpi.kalin.task03;

import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * The Interface View.
 */
public interface View {

	/**
	 * View header.
	 */
	public void viewHeader();
	
	/**
	 * View body.
	 */
	public void viewBody();
	
	/**
	 * View footer.
	 */
	public void viewFooter();
	
	/**
	 * View show.
	 */
	public void viewShow();
	
	/**
	 * View init.
	 */
	public void viewInit();
	
	/**
	 * View save.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void viewSave() throws IOException;
	
	/**
	 * View restore.
	 *
	 * @throws Exception the exception
	 */
	public void viewRestore() throws Exception;
	
}
