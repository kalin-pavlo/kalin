package ua.kharkiv.khpi.kalin.task03;

// TODO: Auto-generated Javadoc
/**
 * The Interface Viewable.
 */
public interface Viewable {
	
	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public View getView();
}
