package ua.kharkiv.khpi.kalin.task08;

import java.awt.Dimension;

import ua.kharkiv.khpi.kalin.task03.ViewResult;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewWindow.
 */
public class ViewWindow extends ViewResult {
	
	/** The Constant POINTS_NUM. */
	private static final int POINTS_NUM = 100;
	
	/** The window. */
	private Window window = null;
	
	/**
	 * Instantiates a new view window.
	 */
	public ViewWindow() {
		super(POINTS_NUM);
		window = new Window(this);
		window.setSize(new Dimension(640, 480));
		window.setTitle ("Result") ;
		window.setVisible(true);
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.ViewResult#viewInit()
	 */
	@Override
	public void viewInit() {
		init((360.0 + Math.random() * 360.0 * 2) / (POINTS_NUM - 1),
			 (360.0 + Math.random() * 360.0 * 2) / (POINTS_NUM - 1), 0);
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.ViewResult#viewShow()
	 */
	@Override
	public void viewShow() {
		super.viewShow();
		window.setVisible(true);
		window.repaint();
	}
}