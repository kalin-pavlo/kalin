package ua.kharkiv.khpi.kalin.task08;

import ua.kharkiv.khpi.kalin.task05.Application;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Application app = Application.getInstance();
		app.run("init window");
		System.exit(0);
	}
}
