package ua.kharkiv.khpi.kalin.task08;

import ua.kharkiv.khpi.kalin.task03.View;
import ua.kharkiv.khpi.kalin.task03.Viewable;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewableWindow.
 */
public class ViewableWindow implements Viewable {
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.Viewable#getView()
	 */
	@Override
	public View getView() {
		return new ViewWindow();
	}
}