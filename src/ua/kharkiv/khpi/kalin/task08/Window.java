package ua.kharkiv.khpi.kalin.task08;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
import ua.kharkiv.khpi.kalin.task03.ViewResult;

// TODO: Auto-generated Javadoc
/**
 * The Class Window.
 */
@SuppressWarnings("serial")
public class Window extends Frame {
	
	/** The Constant BORDER. */
	private static final int BORDER = 20;
	
	/** The view. */
	private ViewResult view;
	
	/**
	 * Instantiates a new window.
	 *
	 * @param view the view
	 */
	public Window(ViewResult view) {
		this.view = view;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				//System.exit(0);
				setVisible(false);
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Window#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {
		Rectangle r = getBounds(), c = new Rectangle();
		r.x += BORDER; r.y += 25 + BORDER;
		r.width -= r.x + BORDER; r.height -= r.y + BORDER;
		c.x = r.x; c.y = r.y + r.height / 2;
		c.width = r.width; c.height = r.height / 2;
		
		g.setColor(Color.LIGHT_GRAY);
		g.setColor(Color.RED);
		g.drawLine(c.x, c.y, c.x + c.width, c.y);
		g.drawLine(c.x, r.y, c.x, r.y + r.height);
		
		double maxVelocity = 0, maxAngle = view.getItems().get(0).getAngle(), 
				scaleVelocity, scaleAngle;
		
		for(TrajectoryParams item : view.getItems()) {
		if (item.getVelocity() > maxVelocity) 
			maxVelocity = item.getVelocity();
		if (Math.abs(item.getAngle()) > maxAngle) 
			maxAngle = Math.abs(item.getAngle());
		}
		
		g.drawString("+" + maxAngle, r.x, r.y);
		g.drawString("-" + maxAngle, r.x, r.y + r.height);
		g.drawString("+" + maxVelocity, c.x + c.width -
		g.getFontMetrics().stringWidth("+" + maxVelocity), c.y);
		
		scaleVelocity = c.width / maxVelocity; 
		scaleAngle    = c.height / maxAngle;
		
		g.setColor(Color.BLUE);
		
		for(TrajectoryParams item : view.getItems()) {
			g.drawOval(
				c.x + (int)(item.getVelocity() * scaleVelocity) - 5,
				c.y - (int)(item.getAngle() * scaleAngle) - 5,
				10, 10
			);
		}
	}
}
