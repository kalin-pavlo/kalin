package ua.kharkiv.khpi.kalin.task01;

import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * Print command-line parameters.
 */
class Main {

	/**
	 * Program entry point.
	 * @param args command-line parameters list
	 */
	public static void main(final String[] args) {
		Arrays.sort(args);
		for (String s: args) {
			System.out.println(s);
		}
	}
}
