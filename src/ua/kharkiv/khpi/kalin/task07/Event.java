package ua.kharkiv.khpi.kalin.task07;

// TODO: Auto-generated Javadoc
//import java.lang.annotation.Retention;

/**
 * The Interface Event.
 */
//@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	
	/**
	 * Value.
	 *
	 * @return the string
	 */
	String value();
}
