package ua.kharkiv.khpi.kalin.task07;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * An asynchronous update interface for receiving notifications
 * about Annotated information as the Annotated is constructed.
 */
public abstract class AnnotatedObserver implements Observer {
	
	/** The handlers. */
	private Map<Object, Method> handlers = new HashMap<Object, Method>();
	
	/**
	 * This method is called when information about an Annotated
	 * which was previously requested using an asynchronous
	 * interface becomes available.
	 */
	public AnnotatedObserver() {
		for (Method m : this.getClass().getMethods()) {
			if (m.isAnnotationPresent(Event.class)) {
				handlers.put(m.getAnnotation(Event.class).value(), m);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task07.Observer#handleEvent(ua.kharkiv.khpi.ponomarenko.task07.Observable, java.lang.Object)
	 */
	@Override
	public void handleEvent(Observable observable, Object event) {
		Method m = handlers.get(event);
		try {
			if (m != null) m.invoke(this, observable);
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}