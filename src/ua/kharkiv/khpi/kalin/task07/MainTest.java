package ua.kharkiv.khpi.kalin.task07;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class MainTest.
 */
public class MainTest {
	
	/** The Constant ITEMS_SIZE. */
	private static final int ITEMS_SIZE = 1000;
	
	/** The generator. */
	private static ItemsGenerator generator = new ItemsGenerator();
	
	/** The sorter. */
	private static ItemsSorter sorter = new ItemsSorter();
	
	/** The observable. */
	private static Items observable = new Items();
	
	/**
	 * Sets the up before class.
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
		observable.addObserver(generator);
		observable.addObserver(sorter);
	}
	
	/**
	 * Test add.
	 */
	@Test
	public void testAdd() {
		observable.getItems().clear();
		observable.add(new Item("Normal Object"));
		observable.add("Normal Object");
		observable.add("");
		observable.add(ITEMS_SIZE);
		for (Item item : observable) {
			assertFalse(item.getData().isEmpty());
		}
		assertEquals(ITEMS_SIZE + 3, observable.getItems().size());
	}
	
	/**
	 * Test add del.
	 */
	@Test
	public void testAddDel() {
		Item tmp;
		observable.getItems().clear();
		observable.add("");
		observable.add(ITEMS_SIZE);
		for (int i = ITEMS_SIZE; i > 0; i--) {
			tmp = observable.getItems().get((new Random()).nextInt(i));
			observable.del(tmp);
		}
		assertEquals(1, observable.getItems().size());
	}
	
	/**
	 * Test sort.
	 */
	@Test
	public void testSort() {
		observable.getItems().clear();
		observable.add(ITEMS_SIZE);
		List<Item> items = new ArrayList<Item>(observable.getItems());
		Collections.sort(items);
		assertEquals(items, observable.getItems());
	}
}
