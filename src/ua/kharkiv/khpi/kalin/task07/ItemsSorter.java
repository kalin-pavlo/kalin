package ua.kharkiv.khpi.kalin.task07;

import java.util.Collections;

// TODO: Auto-generated Javadoc
/**
 * The Class ItemsSorter.
 */
public class ItemsSorter extends AnnotatedObserver {
	
	/** The Constant ITEMS_SORTED. */
	public static final String ITEMS_SORTED = "ITEMS_SORTED";
	
	/**
	 * Items changed.
	 *
	 * @param observable the observable
	 */
	@Event(Items.ITEMS_CHANGED)
	public void itemsChanged(Items observable) {
		Collections.sort(observable.getItems());
		observable.call(ITEMS_SORTED);
	}
	
	/**
	 * Items sorted.
	 *
	 * @param observable the observable
	 */
	@Event(ITEMS_SORTED)
	public void itemsSorted(Items observable) {
		System.out.println(observable.getItems());
	}
	
	/**
	 * Items removed.
	 *
	 * @param observable the observable
	 */
	@Event(Items.ITEMS_REMOVED)
	public void itemsRemoved(Items observable) {
		System.out.println(observable.getItems());
	}
}
