package ua.kharkiv.khpi.kalin.task07;

import ua.kharkiv.khpi.kalin.task05.ConsoleCommand;
import ua.kharkiv.khpi.kalin.task05.Menu;

/**
 * The Class Main.
 */
public class Main {
	
	/**
	 * The Class ConsoleCmd.
	 */
	abstract class ConsoleCmd implements ConsoleCommand {
	
		/** The items. */
		protected Items items;
		
		/** The name. */
		private String name;
		
		/** The key. */
		private char key;
		
		/**
		 * Instantiates a new console cmd.
		 *
		 * @param items the items
		 * @param name the name
		 * @param key the key
		 */
		ConsoleCmd(Items items, String name, char key) {
			this.items = items;
			this.name = name;
			this.key = key;
		}
		
		/* (non-Javadoc)
		 * @see ua.kharkiv.khpi.ponomarenko.task05.ConsoleCommand#getKey()
		 */
		@Override
		public char getKey() {
			return key;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return name;
		}
	}
	
	/**
	 * Run.
	 */
	public void run() {
		Items items = new Items();
		ItemsGenerator generator = new ItemsGenerator();
		ItemsSorter sorter = new ItemsSorter();
		items.addObserver(generator);
		items.addObserver(sorter);
		Menu menu = new Menu();
		menu.add(new ConsoleCmd(items, "'v'iew", 'v') {
			@Override
			public void execute() {
				System.out.println(items.getItems());
			}
		});
		menu.add(new ConsoleCmd(items, "'a'dd", 'a') {
			@Override
			public void execute() {
				items.add("New object");
			}
		});
		menu.add(new ConsoleCmd(items, "'d'el", 'd') {
			@Override
			public void execute() {
				items.del((int)Math.round(Math.random()*(items.getItems().size()-1)));
			}
		});
		menu.execute();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new Main().run();
	}
}