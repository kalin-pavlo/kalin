package ua.kharkiv.khpi.kalin.task07;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Items.
 */
public class Items extends Observable implements Iterable<Item> {
	
	/** The Constant ITEMS_CHANGED. */
	public static final String ITEMS_CHANGED = "ITEMS_CHANGED";
	
	/** The Constant ITEMS_EMPTY. */
	public static final String ITEMS_EMPTY = "ITEMS_EMPTY";
	
	/** The Constant ITEMS_REMOVED. */
	public static final String ITEMS_REMOVED = "ITEMS_REMOVED";
	
	/** The items. */
	private List<Item> items = new ArrayList<Item>();
	
	/**
	 * Adds the.
	 *
	 * @param item the item
	 */
	public void add(Item item) {
		items.add(item);
		if (item.getData().isEmpty()) call(ITEMS_EMPTY);
		else call(ITEMS_CHANGED);
	}
	
	/**
	 * Adds the.
	 *
	 * @param s the s
	 */
	public void add(String s) {
		add(new Item(s));
	}
	
	/**
	 * Adds the.
	 *
	 * @param n the n
	 */
	public void add(int n) {
		if (n > 0) {
			while (n-- > 0) items.add(new Item(""));
			call(ITEMS_EMPTY);
		}
	}
	
	/**
	 * Del.
	 *
	 * @param item the item
	 */
	public void del(Item item) {
		if (item != null) {
			items.remove(item);
			call(ITEMS_REMOVED);
		}
	}
	
	/**
	 * Del.
	 *
	 * @param index the index
	 */
	public void del(int index) {
		if ((index >= 0) && (index < items.size())) {
			items.remove(index);
			call(ITEMS_REMOVED);
		}
	}
	
	/**
	 * Gets the items.
	 *
	 * @return the items
	 */
	public List<Item> getItems() {
		return items;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Item> iterator() {
		return items.iterator();
	}
}
