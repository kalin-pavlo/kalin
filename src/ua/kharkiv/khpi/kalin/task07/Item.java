package ua.kharkiv.khpi.kalin.task07;

// TODO: Auto-generated Javadoc
/**
 * The Class Item.
 */
public class Item implements Comparable<Item> {
	
	/** The data. */
	private String data;
	
	/**
	 * Instantiates a new item.
	 *
	 * @param data the data
	 */
	public Item(String data) {
		this.data = data;
	}
	
	/**
	 * Sets the data.
	 *
	 * @param data the data
	 * @return the string
	 */
	public String setData(String data) {
		return this.data = data;
	}
	
	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Item o) {
		return data.compareTo(o.data);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return data;
	}
}