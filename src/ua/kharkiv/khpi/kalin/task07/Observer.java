package ua.kharkiv.khpi.kalin.task07;

// TODO: Auto-generated Javadoc
/**
 * The Interface Observer.
 */
public interface Observer {
	
	/**
	 * Handle event.
	 *
	 * @param observable the observable
	 * @param event the event
	 */
	public void handleEvent(Observable observable, Object event);
}
