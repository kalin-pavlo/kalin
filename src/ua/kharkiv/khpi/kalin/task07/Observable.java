package ua.kharkiv.khpi.kalin.task07;

import java.util.HashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The Class Observable.
 */
public abstract class Observable {
	
	/** The observers. */
	private Set<Observer> observers = new HashSet<Observer>();
	
	/**
	 * Adds the observer.
	 *
	 * @param observer the observer
	 */
	public void addObserver(Observer observer) {
		observers.add(observer);
	}
	
	/**
	 *  Удаляет наблюдателя; шаблон Observer.
	 *
	 * @param observer объект-наблюдатель
	 */
	public void delObserver(Observer observer) {
		observers.remove(observer);
	}
	
	/**
	 *  Оповещает наблюдателей о событии; шаблон Observer.
	 *
	 * @param event информация о событии
	 */
	public void call(Object event) {
		for (Observer observer : observers) {
			observer.handleEvent(this, event);
		}
	}
}