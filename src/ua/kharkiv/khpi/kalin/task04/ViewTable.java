package ua.kharkiv.khpi.kalin.task04;

import java.util.Formatter;
import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
import ua.kharkiv.khpi.kalin.task03.ViewResult;

/**
 * The Class ViewTable.
 */
public class ViewTable extends ViewResult {
	
	/** The Constant DEFAULT_WIDTH. */
	private static final int DEFAULT_WIDTH = 20;
	
	/** The table width. */
	private int table_width;
	
	/**
	 * Instantiates a new view table.
	 */
	public ViewTable() {
		table_width = DEFAULT_WIDTH;
	}
	
	/**
	 * Instantiates a new view table.
	 *
	 * @param width the width
	 */
	public ViewTable(int width) {
		this.table_width = width;
	}
	
	/**
	 * Instantiates a new view table.
	 *
	 * @param width the width
	 * @param n the n
	 */
	public ViewTable(int width, int n) {
		super(n);
		this.table_width = width;
	}
	
	/**
	 * Sets the width.
	 *
	 * @param width the width
	 * @return the int
	 */
	public int setWidth(int width) {
		return this.table_width = width;
	}
	
	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth() {
		return table_width;
	}
	
	/**
	 * Out line.
	 */
	private void outLine() {
		for(int i = table_width; i > 0; i--) {
			System.out.print("---");
		}
	}
	
	/**
	 * Out line ln.
	 */
	private void outLineLn() {
		outLine();
		System.out.println();
	}
	
	/**
	 * Out header.
	 */
	private void outHeader() {
		Formatter fmt = new Formatter();
		fmt.format("%s\t\t|\t%s\t|\t%s\n", "angle", "velocity", "name");
		System.out.printf(fmt.toString(), "angle", "velocity", "name");
		fmt.close();
	}
	
	/**
	 * Out body.
	 */
	private void outBody() {
		for(TrajectoryParams item : getItems()) {
			System.out.printf("%.3f\t\t|\t%.3f\t\t|\t%s\n", item.getAngle(), item.getVelocity(), item.getName());
		}
	}
	
	/**
	 * Inits the.
	 *
	 * @param width the width
	 */
	public final void init(int width) { // method overloading
		this.table_width = width;
		viewInit();
	}
	
	/**
	 * Inits the.
	 *
	 * @param width the width
	 * @param angle the angle
	 * @param velocity the velocity
	 * @param name the name
	 */
	public final void init(int width, double angle, double velocity, int name) { // method overloading
		this.table_width = width;
		init(angle, velocity, name);
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.ViewResult#init(double, double, int)
	 */
	@Override
	public void init(double angle, double velocity, int name) { // method overriding
		System.out.print("Initialization... ");
		super.init(angle, velocity, name);
		System.out.println("done. ");
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.ViewResult#viewHeader()
	 */
	@Override
	public void viewHeader() {
		outHeader();
		outLineLn();
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.ViewResult#viewBody()
	 */
	@Override
	public void viewBody() {
		outBody();
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.ViewResult#viewFooter()
	 */
	@Override
	public void viewFooter() {
		outLineLn();
	}
}
