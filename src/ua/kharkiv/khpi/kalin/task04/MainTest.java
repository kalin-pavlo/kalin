package ua.kharkiv.khpi.kalin.task04;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.IOException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;

// TODO: Auto-generated Javadoc
/**
 * The Class TrajectoryTests.
 */
public class MainTest {
	
	/**
	 * Test calc.
	 */
	@Test
	public void testCalc() {
		ViewTable tbl = new ViewTable(10, 5);
		assertEquals(10, tbl.getWidth());
		assertEquals(5, tbl.getItems().size());
		tbl.init(40, 90.0, 15);
		
		TrajectoryParams item = new TrajectoryParams();
		
		int ctr = 1;
		
		item.setAngle(40.0);
		item.setVelocity(90.0);
		item.setName("Path calc #15");
		
		assertEquals(item.getAngle(), tbl.getItems().get(ctr).getAngle(), 
				item.getAngle() + " should be equal to " + 
						tbl.getItems().get(ctr).getAngle());
		
		assertEquals(item.getVelocity(), 
				tbl.getItems().get(ctr).getVelocity(), 
				item.getAngle() + " should be equal to " + 
						tbl.getItems().get(ctr).getVelocity());
		
		assertEquals(item.getName(), tbl.getItems().get(ctr-1).getName(), 
				item.getName() + " should be equal to " + tbl.getItems().get(ctr-1).getName());
	}
	
	/**
	 * Test restore.
	 */
	@Test
	public void testRestore() {
		ViewTable tbl1 = new ViewTable(10, 1000);
		ViewTable tbl2 = new ViewTable();
		
		tbl1.init(Math.random() * 180.0, 
				Math.random() * 90.0, 
				(int)Math.random()+1);
		
		try {
			tbl1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		
		try {
			tbl2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
