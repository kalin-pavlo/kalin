package ua.kharkiv.khpi.kalin.task04;

import ua.kharkiv.khpi.kalin.task03.View;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main extends ua.kharkiv.khpi.kalin.task03.Main {

	/**
	 * Instantiates a new main.
	 *
	 * @param view the view
	 */
	public Main(View view) {
		super(view);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Main main = new Main(new ViewableTable().getView());
		main.menu();
	}
}