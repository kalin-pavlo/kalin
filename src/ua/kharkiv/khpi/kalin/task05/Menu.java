package ua.kharkiv.khpi.kalin.task05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Menu.
 */
public class Menu implements Command {
	
	/** The menu. */
	private List<ConsoleCommand> menu = new ArrayList<ConsoleCommand>();
	
	/**
	 * Adds the.
	 *
	 * @param command the command
	 * @return the console command
	 */
	public ConsoleCommand add(ConsoleCommand command) {
		menu.add(command);
		return command;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "Enter command...\n";
		for (ConsoleCommand c : menu) {
			s += c + ", ";
		}
		s += "'q'uit: ";
		return s;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		String s = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		menu: while (true) {
			do {
				System.out.print(this);
				try {
					s = in.readLine();
				} catch (IOException e) {
					System.err.println("Error: " + e);
					System.exit(0);
				}
			} while (s.length() != 1);
			
			char key = s.charAt(0);
			
			if (key == 'q') {
				System.out.println("Exit.");
				break menu;
			}
			
			for (ConsoleCommand c : menu) {
				if (s.charAt(0) == c.getKey()) {
					c.execute();
					continue menu;
				}
			}
			System.out.println("Wrong command.");
			continue menu;
		}
	}
}