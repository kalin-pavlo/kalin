package ua.kharkiv.khpi.kalin.task05;

import static org.junit.Assert.*;
import org.junit.Test;

import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
import ua.kharkiv.khpi.kalin.task03.ViewResult;

// TODO: Auto-generated Javadoc
/**
 * The Class MainTest.
 */
public class MainTest {
	
	/**
	 * Test execute.
	 */
	@Test
	public void testExecute() {
		ChangeItemCommand cmd = new ChangeItemCommand();
		cmd.setItem(new TrajectoryParams());
		double velocity, angle, offset;
		for (int ctr = 0; ctr < 1000; ctr++) {
			cmd.getItem().setVelocity(velocity = Math.random() * 100.0);
			cmd.getItem().setAngle(angle = Math.random() * 100.0);
			cmd.setOffset(offset = Math.random() * 100.0);
			cmd.execute();
			assertEquals(velocity, cmd.getItem().getVelocity(), .1e-10);
			assertEquals(angle * offset, cmd.getItem().getAngle(), .1e-10);
		}
	}
	
	/**
	 * Test change console command.
	 */
	@Test
	public void testChangeConsoleCommand() {
		ChangeConsoleCommand cmd = new ChangeConsoleCommand(new ViewResult());
		cmd.getView().viewInit();
		cmd.execute();
		assertEquals("'c'hange", cmd.toString());
		assertEquals('c', cmd.getKey());
	}
}