package ua.kharkiv.khpi.kalin.task05;

import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;

// TODO: Auto-generated Javadoc
/**
 * The Class ChangeItemCommand.
 */
public class ChangeItemCommand implements Command {
	
	/** The item. */
	private TrajectoryParams item;
	
	/** The offset. */
	private double offset;
	
	/**
	 * Sets the item.
	 *
	 * @param item the item
	 * @return the trajectory params
	 */
	public TrajectoryParams setItem(TrajectoryParams item) {
		return this.item = item;
	}
	
	/**
	 * Gets the item.
	 *
	 * @return the item
	 */
	public TrajectoryParams getItem() {
		return item;
	}
	
	/**
	 * Sets the offset.
	 *
	 * @param offset the offset
	 * @return the double
	 */
	public double setOffset(double offset) {
		return this.offset = offset;
	}
	
	/**
	 * Gets the offset.
	 *
	 * @return the offset
	 */
	public double getOffset() {
		return offset;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		item.setAngle(item.getAngle() * offset);
	}
}
