package ua.kharkiv.khpi.kalin.task05;

import ua.kharkiv.khpi.kalin.task03.View;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewConsoleCommand.
 */
public class ViewConsoleCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;
	
	/**
	 * Instantiates a new view console command.
	 *
	 * @param view the view
	 */
	public ViewConsoleCommand(View view) {
		this.view = view;
	}

	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.ConsoleCommand#getKey()
	 */
	@Override
	public char getKey() {
		return 'v';
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "'v'iew";
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("View current.");
		view.viewShow();
	}
}
