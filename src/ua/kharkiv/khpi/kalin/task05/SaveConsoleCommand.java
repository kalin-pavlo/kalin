package ua.kharkiv.khpi.kalin.task05;

import java.io.IOException;

import ua.kharkiv.khpi.kalin.task03.View;

// TODO: Auto-generated Javadoc
/**
 * The Class SaveConsoleCommand.
 */
public class SaveConsoleCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;
	
	/**
	 * Instantiates a new save console command.
	 *
	 * @param view the view
	 */
	public SaveConsoleCommand(View view) {
		this.view = view;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.ConsoleCommand#getKey()
	 */
	@Override
	public char getKey() {
		return 's';
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "'s'ave";
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Save current.");
		try {
			view.viewSave();
		} catch (IOException e) {
			System.err.println("Serialization error: " + e);
		}
		view.viewShow();
	}
}
