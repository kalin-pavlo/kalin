package ua.kharkiv.khpi.kalin.task05;

import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
import ua.kharkiv.khpi.kalin.task03.View;
import ua.kharkiv.khpi.kalin.task03.ViewResult;

// TODO: Auto-generated Javadoc
/**
 * The Class ChangeConsoleCommand.
 */
public class ChangeConsoleCommand extends ChangeItemCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;
	
	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public View getView() {
		return view;
	}
	
	/**
	 * Sets the view.
	 *
	 * @param view the view
	 * @return the view
	 */
	public View setView(View view) {
		return this.view = view;
	}
	
	/**
	 * Instantiates a new change console command.
	 *
	 * @param view the view
	 */
	public ChangeConsoleCommand(View view) {
		this.view = view;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.ConsoleCommand#getKey()
	 */
	@Override
	public char getKey() {
		return 'c';
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "'c'hange";
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.ChangeItemCommand#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Change item: scale factor " + setOffset(Math.random() * 100.0));
		for (TrajectoryParams item : ((ViewResult)view).getItems()) {
			super.setItem(item);
			super.execute();
		}
		view.viewShow();
	}
}
