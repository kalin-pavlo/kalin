package ua.kharkiv.khpi.kalin.task05;

// TODO: Auto-generated Javadoc
/**
 * The Interface Command.
 */
public interface Command {
	
	/**
	 * Execute.
	 */
	public void execute();
}
