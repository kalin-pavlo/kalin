package ua.kharkiv.khpi.kalin.task05;

import ua.kharkiv.khpi.kalin.task03.View;

import ua.kharkiv.khpi.kalin.task04.ViewableTable;
import ua.kharkiv.khpi.kalin.task08.ViewWindow;

// TODO: Auto-generated Javadoc
/**
 * The Class Application.
 */
public class Application {
	
	/** The instance. */
	private static Application instance = new Application();
	
	/**
	 * Instantiates a new application.
	 */
	private Application() {}
	
	/**
	 * Gets the single instance of Application.
	 *
	 * @return single instance of Application
	 */
	public static Application getInstance() {
		return instance;
	}
	
	/** The view. */
	private View view = new ViewableTable().getView();
	
	/** The menu. */
	private Menu menu = new Menu();
	
	/**
	 * Run.
	 *
	 * @param isWindow the is window
	 */
	public void run(String isWindow) {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
	
		menu.add(new ChangeConsoleCommand(view));
		menu.add(new SaveConsoleCommand(view));
		menu.add(new RestoreConsoleCommand(view));
		
		if(isWindow == "init window") {
			ViewWindow window = new ViewWindow();
			window.viewInit();
			window.viewShow();
		}
		
		menu.execute();
	}
}