package ua.kharkiv.khpi.kalin.task05;

import ua.kharkiv.khpi.kalin.task03.View;

/**
 * The Class GenerateConsoleCommand.
 */
public class GenerateConsoleCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;
	
	/**
	 * Instantiates a new generate console command.
	 *
	 * @param view the view
	 */
	public GenerateConsoleCommand(View view) {
		this.view = view;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.ConsoleCommand#getKey()
	 */
	@Override
	public char getKey() {
		return 'g';
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "'g'enerate";
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Random generation.");
		view.viewInit();
		view.viewShow();
	}
}
