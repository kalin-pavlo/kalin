package ua.kharkiv.khpi.kalin.task06;

import java.util.concurrent.TimeUnit;

import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
import ua.kharkiv.khpi.kalin.task03.ViewResult;
import ua.kharkiv.khpi.kalin.task05.Command;

/**
 * The Class MinMaxCommand.
 */
public class MinMaxCommand implements Command /*, Runnable */ {
	
	/** The result min. */
	private int resultMin = -1;
	
	/** The result max. */
	private int resultMax = -1;
	
	/** The progress. */
	private int progress = 0;
	
	/** The view result. */
	private ViewResult viewResult;
	
	/**
	 * Gets the view result.
	 *
	 * @return the view result
	 */
	public ViewResult getViewResult() {
		return viewResult;
	}
	
	/**
	 * Sets the view result.
	 *
	 * @param viewResult the view result
	 * @return the view result
	 */
	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}
	
	/**
	 * Instantiates a new min max command.
	 *
	 * @param viewResult the view result
	 */
	public MinMaxCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}
	
	/**
	 * Gets the result min.
	 *
	 * @return the result min
	 */
	public int getResultMin() {
		return resultMin;
	}
	
	/**
	 * Gets the result max.
	 *
	 * @return the result max
	 */
	public int getResultMax() {
		return resultMax;
	}
	
	/**
	 * Running.
	 *
	 * @return true, if successful
	 */
	public boolean running() {
		return progress < 100;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		progress = 0;
		System.out.println("MinMax executed...");
		int idx = 0, size = viewResult.getItems().size();
		for (TrajectoryParams item : viewResult.getItems()) {
			if (item.getAngle() < 0) {
				if ((resultMax == -1) ||
						(viewResult.getItems().get(resultMax).getAngle() < item.getAngle())) {
					resultMax = idx;
				}
			} else {
				if ((resultMin == -1) ||
						(viewResult.getItems().get(resultMin).getAngle() > item.getAngle())) {
					resultMin = idx;
				}
			}
			idx++;
			progress = idx * 100 / size;
			if (idx % (size / 5) == 0) {
				System.out.println("MinMax " + progress + "%");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(5000 / size);
			} catch (InterruptedException e) {
				System.err.println(e);
			}
		}
		System.out.print("MinMax done. ");
		if (resultMin > -1) {
			System.out.print("Min positive #" + resultMin + " found: " +
					String.format("%.2f.",
							viewResult.getItems().get(resultMin).getAngle()));
		} else {
			System.out.print("Min positive not found.");
		}
		if (resultMax > -1) {
			System.out.println(" Max negative #" + resultMax + " found: " +
					String.format("%.2f.",
							viewResult.getItems().get(resultMax).getAngle()));
		} else {
			System.out.println(" Max negative item not found.");
		}
		progress = 100;
	}
	
	/**
	@Override
	public void run() {
	execute();
	}
	/**/
}
