package ua.kharkiv.khpi.kalin.task06;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import ua.kharkiv.khpi.kalin.task03.ViewResult;

// TODO: Auto-generated Javadoc
/**
 * The Class MainTest.
 */
public class MainTest {

	/** The Constant N. */
	private final static int N = 1000;
	
	/** The view. */
	private static ViewResult view = new ViewResult(N);
	
	/** The max 1. */
	private static MaxCommand max1 = new MaxCommand(view);
	
	/** The max 2. */
	private static MaxCommand max2 = new MaxCommand(view);
	
	/** The avg 1. */
	private static AvgCommand avg1 = new AvgCommand(view);
	
	/** The avg 2. */
	private static AvgCommand avg2 = new AvgCommand(view);
	
	/** The min 1. */
	private static MinMaxCommand min1 = new MinMaxCommand(view);
	
	/** The min 2. */
	private static MinMaxCommand min2 = new MinMaxCommand(view);
	
	/** The queue. */
	CommandQueue queue = new CommandQueue();
	
	/**
	 * Sets the up before class.
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
		view.viewInit();
		assertEquals(N, view.getItems().size());
	}

	/**
	 * Tear down after class.
	 */
	@AfterClass
	public static void tearDownAfterClass() {
		assertEquals(max1.getResult(), max2.getResult());
		assertEquals(avg1.getResult(), avg2.getResult(), .1e-10);
		assertEquals(min1.getResultMax(), min2.getResultMax());
		assertEquals(min1.getResultMin(), min2.getResultMin());
	}
	
	
	/**
	 * Test max.
	 */
	@Test
	public void testMax() {
		max1.execute();
		assertTrue( max1.getResult() > -1);
	}
	
	/**
	 * Test avg.
	 */
	@Test
	public void testAvg() {
		avg1.execute();
		assertTrue( avg1.getResult() != 0.0);
	}
	
	/**
	 * Test min.
	 */
	@Test
	public void testMin() {
		min1.execute();
		assertTrue(min1.getResultMin() > -1);
	}
	
	/**
	 * Test max queue.
	 */
	@Test
	public void testMaxQueue() {
		queue.put(max2);
		try {
			while (max2.running()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
			queue.shutdown();
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			fail(e.toString());
		}
	}
	
	/**
	 *  Проверка основной функциональности класса
	 * {@linkplain CommandQueue} с задачей {@linkplain AvgCommand}.
	 */
	@Test
	public void testAvgQueue() {
		queue.put(avg2);
		try {
			while (avg2.running()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
			queue.shutdown();
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			fail(e.toString());
		}
	}
	
	/**
	 * Test min queue.
	 */
	@Test
	public void testMinQueue() {
		queue.put(min2);
		try {
			while (min2.running()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
		queue.shutdown();
		TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			fail(e.toString());
		}
	}
}
