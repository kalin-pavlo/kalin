package ua.kharkiv.khpi.kalin.task06;

import java.util.concurrent.TimeUnit;

import ua.kharkiv.khpi.kalin.task03.ViewResult;
import ua.kharkiv.khpi.kalin.task05.Command;

// TODO: Auto-generated Javadoc
/**
 * The Class MaxCommand.
 */
public class MaxCommand implements Command /*, Runnable */ {
	
	/** The result. */
	private int result = -1;
	
	/** The progress. */
	private int progress = 0;
	
	/** The view result. */
	private ViewResult viewResult;
	
	/**
	 * Gets the view result.
	 *
	 * @return the view result
	 */
	public ViewResult getViewResult() {
		return viewResult;
	}
	
	/**
	 * Sets the view result.
	 *
	 * @param viewResult the view result
	 * @return the view result
	 */
	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}
	
	/**
	 * Instantiates a new max command.
	 *
	 * @param viewResult the view result
	 */
	public MaxCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}
	
	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public int getResult() {
		return result;
	}
	
	/**
	 * Running.
	 *
	 * @return true, if successful
	 */
	public boolean running() {
		return progress < 100;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		progress = 0;
		System.out.println("Max executed...");
		int size = viewResult.getItems().size();
		result = 0;
		for (int idx = 1; idx < size; idx++) {
			if (viewResult.getItems().get(result).getAngle() <
					viewResult.getItems().get(idx).getAngle()) {
				result = idx;
			}
			progress = idx * 100 / size;
			if (idx % (size / 3) == 0) {
				System.out.println("Max " + progress + "%");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(3000 / size);
			} catch (InterruptedException e) {
				System.err.println(e);
			}
		}
		System.out.println("Max done. Item #" + result +
		" found: " + viewResult.getItems().get(result));
		progress = 100;
	}

	/**
	@Override
	public void run() {
	execute();
	}
	/**/
}