package ua.kharkiv.khpi.kalin.task06;

import java.util.concurrent.TimeUnit;

import ua.kharkiv.khpi.kalin.task02.TrajectoryParams;
import ua.kharkiv.khpi.kalin.task03.ViewResult;
import ua.kharkiv.khpi.kalin.task05.Command;

// TODO: Auto-generated Javadoc
/**
 * The Class AvgCommand.
 */
public class AvgCommand implements Command /*, Runnable */ {

	/** The result. */
	private double result = 0.0;
	
	/** The progress. */
	private int progress = 0;
	
	/** The view result. */
	private ViewResult viewResult;
	
	/**
	 * Gets the view result.
	 *
	 * @return the view result
	 */
	public ViewResult getViewResult() {
		return viewResult;
	}
	
	/**
	 * Sets the view result.
	 *
	 * @param viewResult the view result
	 * @return the view result
	 */
	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}
	
	/**
	 * Instantiates a new avg command.
	 *
	 * @param viewResult the view result
	 */
	public AvgCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}
	
	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public double getResult() {
		return result;
	}
	
	/**
	 * Running.
	 *
	 * @return true, if successful
	 */
	public boolean running() {
		return progress < 100;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		progress = 0;
		System.out.println("Average executed...");
		result = 0.0;
		
		int idx = 1, size = viewResult.getItems().size();
		
		for (TrajectoryParams item : viewResult.getItems()) {
			
			result += item.getAngle();
			progress = idx * 100 / size;
			
			if (idx++ % (size / 2) == 0) {
				System.out.println("Average " + progress + "%");
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(2000 / size);
			} catch (InterruptedException e) {
				System.err.println(e);
			}
		}
		result /= size;
		System.out.println("Average done. Result = " + String.format("%.2f",result));
		progress = 100;
	}
	
	/**
	@Override
	public void run() {
	execute();
	}
	/**/

}
