package ua.kharkiv.khpi.kalin.task06;

import ua.kharkiv.khpi.kalin.task05.Command;

// TODO: Auto-generated Javadoc
/**
 * The Interface Queue.
 */
public interface Queue {
	
	/**
	 * Put.
	 *
	 * @param cmd the cmd
	 */
	void put(Command cmd);
	
	/**
	 * Take.
	 *
	 * @return the command
	 */
	Command take();
}
