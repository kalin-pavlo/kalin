package ua.kharkiv.khpi.kalin.task06;

import java.util.concurrent.TimeUnit;

import ua.kharkiv.khpi.kalin.task03.View;
import ua.kharkiv.khpi.kalin.task03.ViewResult;
import ua.kharkiv.khpi.kalin.task05.ConsoleCommand;

// TODO: Auto-generated Javadoc
/**
 * The Class ExecuteConsoleCommand.
 */
public class ExecuteConsoleCommand implements ConsoleCommand {

	/** The view. */
	private View view;
	
	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public View getView() {
		return view;
	}
	
	/**
	 * Sets the view.
	 *
	 * @param view the view
	 * @return the view
	 */
	public View setView(View view) {
		return this.view = view;
	}
	
	/**
	 * Instantiates a new execute console command.
	 *
	 * @param view the view
	 */
	public ExecuteConsoleCommand(View view) {
		this.view = view;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.ConsoleCommand#getKey()
	 */
	@Override
	public char getKey() {
		return 'e';
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "'e'xecute";
	}

	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
	
		CommandQueue queue1 = new CommandQueue();
		CommandQueue queue2 = new CommandQueue();
		/**
		ExecutorService exec1 = Executors.newSingleThreadExecutor();
		ExecutorService exec2 = Executors.newSingleThreadExecutor();
		/**/
		MaxCommand maxCommand = new MaxCommand((ViewResult)view);
		AvgCommand avgCommand = new AvgCommand((ViewResult)view);
		MinMaxCommand minMaxCommand = new MinMaxCommand((ViewResult)view);
		
		System.out.println("Execute all threads...");
		/**
		exec1.execute(minMaxCommand);
		exec2.execute(maxCommand);
		exec2.execute(avgCommand);
		/**/
		queue1.put(minMaxCommand);
		queue2.put(maxCommand);
		queue2.put(avgCommand);
		/**/
		try {
			while (avgCommand.running() ||
			maxCommand.running() ||
			minMaxCommand.running()) {
			TimeUnit.MILLISECONDS.sleep(100);
			}
			/**
			exec1.shutdown();
			exec2.shutdown();
			/**/
			queue1.shutdown();
			queue2.shutdown();
			/**/
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			System.err.println(e);
		}
		System.out.println("All done.");
	}
}
