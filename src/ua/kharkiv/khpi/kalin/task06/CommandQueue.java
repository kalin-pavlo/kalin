package ua.kharkiv.khpi.kalin.task06;

import java.util.Vector;

import ua.kharkiv.khpi.kalin.task05.Command;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandQueue.
 */
public class CommandQueue implements Queue {
	
	/** The tasks. */
	private Vector<Command> tasks;
	
	/** The waiting. */
	private boolean waiting;
	
	/** The shutdown. */
	private boolean shutdown;
	
	/**
	 * Shutdown.
	 */
	public void shutdown() {
		shutdown = true;
	}
	
	/**
	 * Instantiates a new command queue.
	 */
	public CommandQueue() {
		tasks = new Vector<Command>();
		waiting = false;
		new Thread(new Worker()).start();
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task06.Queue#put(ua.kharkiv.khpi.ponomarenko.task05.Command)
	 */
	@Override
	public void put(Command r) {
		tasks.add(r);
		if (waiting) {
			synchronized (this) {
			notifyAll();
		}
	}
}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task06.Queue#take()
	 */
	@Override
	public Command take() {
		if (tasks.isEmpty()) {
			synchronized (this) {
				waiting = true;
				try {
					wait();
				} catch (InterruptedException ie) {
					waiting = false;
				}
			}
		}
		return (Command)tasks.remove(0);
	}

	/**
	 * The Class Worker.
	 */
	private class Worker implements Runnable {
		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			while (!shutdown) {
				Command r = take();
				r.execute();
			}
		}
	}
}
