package ua.kharkiv.khpi.kalin.task06;

import ua.kharkiv.khpi.kalin.task03.View;
import ua.kharkiv.khpi.kalin.task03.ViewableResult;
import ua.kharkiv.khpi.kalin.task05.ChangeConsoleCommand;
import ua.kharkiv.khpi.kalin.task05.GenerateConsoleCommand;
import ua.kharkiv.khpi.kalin.task05.Menu;
import ua.kharkiv.khpi.kalin.task05.ViewConsoleCommand;

/**
 * The Class Main.
 */
public class Main {

	/** The view. */
	private View view = new ViewableResult().getView();
	
	/** The menu. */
	private Menu menu = new Menu();
	
	/**
	 * Run.
	 */
	public void run() {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
		menu.add(new ChangeConsoleCommand(view));
		menu.add(new ExecuteConsoleCommand(view));
		menu.execute();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Main main = new Main();
		main.run();
	}
}