package ua.kharkiv.khpi.kalin.task02;

import static org.junit.Assert.*;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class TrajectoryTests.
 */
public class TrajectoryTests {
	
	/**
	 * Should return correct params.
	 */
	@Test
    public void shouldReturnCorrectParams() {
		
		 String name     = "test params";
		 double angle    = 54;
		 double velocity = 352.45;
		 double path     = 0;
		 
		 TrajectoryParams params = new TrajectoryParams(name, velocity, angle);
		 
		 TrajectoryCalc calc = new TrajectoryCalc(params);
		 calc.calcAndSavePath();
		 path = params.getPath();
		 
		 final double G = 9.81;
		 double calc_path = Math.sin(angle*Math.PI/90)*
				  (velocity*velocity/G);
		 
		 assertEquals(name, params.getName(), 
				 name + " should be equal to " + params.getName());
	 }
	
	/**
	 * Should load correct params.
	 */
	@Test
    public void shouldLoadCorrectParams() {
		
		 String name     = "test params";
		 double angle    = 54;
		 double velocity = 352.45;
		 
		 TrajectoryParams params = new TrajectoryParams(name, velocity, angle);
		 
		 TrajectoryManip manip   = new TrajectoryManip(params);
		 manip.storeParams();
		 
		 TrajectoryParams get_params = manip.loadParams();
		 assertEquals(true, true);
	 }
	
	
}
