package ua.kharkiv.khpi.kalin.task02;

import java.io.Serializable;

/**
 * The Class Trajectory.
 */
public class TrajectoryParams implements Serializable {
	
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 187529927958255050L;

	/** The name. */
    private transient String name;
    
    /** The velocity. */
    private double velocity;
    
    /** The angle. */
    private double angle;
    
    /** The path. */
    private double path;
 
    /**
     * Instantiates a new trajectory params.
     */
    public TrajectoryParams() {
    	
    }
    
    /**
     * Instantiates a new trajectory.
     *
     * @param name the name
     * @param velocity the velocity
     * @param angle the angle
     */
    public TrajectoryParams(String name, double velocity, double angle) {
        this.name = name;
        this.velocity = velocity;
        this.angle = angle;
    }
 
    /**
     * Sets the velocity.
     *
     * @param velocity the new velocity
     */
    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }
 
    /**
     * Sets the angle.
     *
     * @param angle the new angle
     */
    public void setAngle(double angle) {
        this.angle = angle;
    }
    
    /**
     * Sets the path.
     *
     * @param path the new path
     */
    public void setPath(double path) {
    	this.path = path;
    }
    
    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
    	this.name = name;
    }
    
    /**
     * Gets the velocity.
     *
     * @return the velocity
     */
    public double getVelocity() {
    	return this.velocity;
    }
    
    /**
     * Gets the angle.
     *
     * @return the angle
     */
    public double getAngle() {
    	return this.angle;
    }
    
    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
    	return this.name;
    }
    
    /**
     * Gets the path.
     *
     * @return the path
     */
    public double getPath() {
    	return this.path;
    }
}
