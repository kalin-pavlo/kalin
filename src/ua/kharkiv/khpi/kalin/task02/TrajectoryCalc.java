package ua.kharkiv.khpi.kalin.task02;

// TODO: Auto-generated Javadoc
/**
 * The Class TrajectoryCalc.
 */
public class TrajectoryCalc {
	
	/** The Constant G. */
	public static final double G = 9.81;
	
	/** The params. */
	private TrajectoryParams params;
    
	/**
	 * Instantiates a new trajectory calc.
	 *
	 * @param params the params
	 */
	public TrajectoryCalc(TrajectoryParams params) {
		this.params = params;
	}
	
	/**
	 * Sets the source params.
	 *
	 * @param params the new source params
	 */
	public void setSourceParams(TrajectoryParams params) {
		this.params = params;
	}
	
	/**
	 * Calc new path.
	 */
    public void calcAndSavePath() {
    	double angle    = params.getAngle();
    	double velocity = params.getVelocity();
        double path     = Math.sin(angle*Math.PI/90)*
        						  (velocity*velocity/G);
        params.setPath(path);
    }
}
