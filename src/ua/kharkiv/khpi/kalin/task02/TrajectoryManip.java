package ua.kharkiv.khpi.kalin.task02;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

// TODO: Auto-generated Javadoc
/**
 * The Class TrajectoryDisplay.
 */
public class TrajectoryManip {
	
	/** The params. */
	private TrajectoryParams params;
	
	/** The Constant MPH_IN_1MPS. */
	private static final double MPH_IN_1MPS       = 2.237;
	
	/** The Constant OCTANTS_IN_1GRAD. */
	private static final double OCTANTS_IN_1GRAD  = 0.02;
	
	/** The Constant ARPENTS_IN_1METER. */
	private static final double ARPENTS_IN_1METER = 0.01709;
	
	/**
	 * Instantiates a new trajectory manip.
	 *
	 * @param params the params
	 */
	public TrajectoryManip(TrajectoryParams params) {
		this.params = params;
	}
	
	/**
	 * Sets the source params.
	 *
	 * @param params the new source params
	 */
	public void setSourceParams(TrajectoryParams params) {
		this.params = params;
	}
	
	/**
	 * Store params.
	 */
	public void storeParams() {
		
		try {
			
			FileOutputStream fileOutputStream = new FileOutputStream("params.data");
			ObjectOutputStream output = new ObjectOutputStream(fileOutputStream);
			
			output.writeObject(params);
			output.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Load params.
	 *
	 * @return the trajectory params
	 */
	public TrajectoryParams loadParams() {
		
		TrajectoryParams get_params = null;
		
		try {
			
			FileInputStream fileInputStream = new FileInputStream(new File("params.data"));
			ObjectInputStream input = new ObjectInputStream(fileInputStream);
			
			get_params = (TrajectoryParams) input.readObject();
			input.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return get_params;
		
	}
	
	/**
	 * Display path.
	 *
	 * @return the string
	 */
    public String displayPath() {
    	String name     = params.getName();
    	double velocity = params.getVelocity();
    	double angle    = params.getAngle();
    	double path     = params.getPath();
    	
        return String.format("%n -- Trajectory %s : -- %n"
        		+ "* initial speed   - %.3f m/c  \t --> %.3f mph %n"
                + "* launch angle    - %.2f grad \t\t --> %.2f octants %n"
                + "* flight distance - %.2f m    \t --> %.2f arpents %n" 
                + "%n------------------------------------------%n",
                name, 
                velocity, velocity * MPH_IN_1MPS,
                angle, angle * OCTANTS_IN_1GRAD,
                path, path * ARPENTS_IN_1METER);
    }
}
