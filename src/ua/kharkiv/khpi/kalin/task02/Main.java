package ua.kharkiv.khpi.kalin.task02;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		TrajectoryParams tr_params1 = new TrajectoryParams("1st flight distance", 3120, 83);
        TrajectoryParams tr_params2 = new TrajectoryParams("2nd flight distance", 1510, 46);
        
        TrajectoryCalc tr_calc1 = new TrajectoryCalc(tr_params1);
        tr_calc1.calcAndSavePath();
        
        TrajectoryManip tr_manip1 = new TrajectoryManip(tr_params1);
    
        System.out.println(tr_manip1.displayPath());
        
        TrajectoryCalc tr_calc2 = new TrajectoryCalc(tr_params2);
        tr_calc2.calcAndSavePath();
        
        TrajectoryManip tr_manip2 = new TrajectoryManip(tr_params2);
        
        System.out.println(tr_manip2.displayPath());
        
        tr_manip2.storeParams();
        
        TrajectoryParams get_params = tr_manip2.loadParams();
        TrajectoryManip tr_get_manip = new TrajectoryManip(get_params);
      
        System.out.println("--Retrieved parameters--");
        System.out.println(tr_get_manip.displayPath());
	}
}
